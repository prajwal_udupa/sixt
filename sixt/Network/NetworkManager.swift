//
//  NetworkManager.swift
//  sixt
//
//  Created by Prajwal Udupa on 05/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import UIKit
class NetworkManager {
    
    //Creating Singleton
    static let sharedInstance = NetworkManager()
    private var sessionManager:SessionManager
    private var imageCache:AutoPurgingImageCache
    private var cacheSessionManager:SessionManager
    private init() {
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = nil
        sessionManager = Alamofire.SessionManager(configuration: configuration)
        cacheSessionManager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
        imageCache = AutoPurgingImageCache()
    }
    
    
    //Networking
    func requestWith(data:Dictionary<String, AnyObject>,url:String,completionBlock:@escaping networkResultBlock) -> DataRequest {
        return sessionManager.request(url).responseJSON(completionHandler: { (response) in
            
            //Handle Failure
            if (response.result.isFailure) {
                if let error = response.error {
                    completionBlock(nil,error)
                }
                else
                {
                    //We do not have any concrete information on why the request failed. Letting the controllers handle it
                    completionBlock(nil,nil)
                }
            }
            else
            {
                if let array = response.result.value {
                    completionBlock([Constants.responseKey:array],nil)
                }
                
            }
            
        })
        
    }
    
    
    func requestCarImage(car:Car,completionBlock:@escaping networkResultBlock) -> DataRequest? {
        if let cachedAvatarImage = imageCache.image(for: URLRequest(url: URL(string: car.carImageURL)!), withIdentifier: car.carImageURL) {
            completionBlock([Constants.responseKey:cachedAvatarImage],nil)
            
        }
        else
        {
            return cacheSessionManager.request(car.carImageURL).responseData(completionHandler: { (data) in
                if let imageData = data.value ,let image = UIImage(data:imageData){
                    self.imageCache.add(image, for: URLRequest(url: URL(string: car.carImageURL)!), withIdentifier: car.carImageURL)

                    completionBlock([Constants.responseKey:image],nil)
                    
                }
            })
        }
        return nil
    }
    
} 
