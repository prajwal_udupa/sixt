//
//  ViewController.swift
//  sixt
//
//  Created by Prajwal Udupa on 05/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import UIKit

class FirstImpressionController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,FirstImpressionViewProtocol {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModeChangedBlock:viewModeChangedBlock?
    var contentRefreshRequestedBlock: contentRefreshRequestedBlock?
    var contentController:UIViewController?
    var currentViewMode:ViewMode = .ListView
    @IBOutlet var firstImpressionView: FirstImpressionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        self.firstImpressionView.delegate = self
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func requestContent() {
        self.firstImpressionView.viewMode = .FetchingContent
        self.contentRefreshRequestedBlock?(self)
        
    }
    func changeContentControl(controller:UIViewController) {
        
        if let currentController = self.contentController {
            currentController.view.removeFromSuperview()
            currentController.removeFromParentViewController()
            self.contentController = nil
        }
        self.firstImpressionView.updateInterfaceWithView(view:controller.view)
        self.addChildViewController(controller)
        
        
    }
    func changeToNoResponse() {
        self.firstImpressionView.viewMode = .NoResponse
    }
    
    func viewModeChanged(mode:ViewMode) {
        if DataManager.sharedInstance.cars == nil || DataManager.sharedInstance.cars?.count == 0 {
            return
        }
        self.currentViewMode = mode
        viewModeChangedBlock?(mode)
        collectionView.reloadData()
    }
    
    func reloadView(){
        if (DataManager.sharedInstance.selectedCategory == nil) {
            DataManager.sharedInstance.selectedCategory = DataManager.sharedInstance.cars?.first?.modelName
        }
        if(DataManager.sharedInstance.cars == nil) {
            self.firstImpressionView.viewMode = .NoResponse
        }
        else
        {
            self.firstImpressionView.viewMode = .ResponseView
        }
        collectionView.reloadData()
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let filteredCars = DataManager.sharedInstance.filteredCars {
            return Array(filteredCars.keys).count
        }
        return 0
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let filteredCars = DataManager.sharedInstance.filteredCars {
            if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as? CategoryCell {
                if Array(filteredCars.keys).count > indexPath.row {
                    cell.setupWithData(category: Array(filteredCars.keys)[indexPath.row])
                    return cell
                }
                return cell
                
            }
            return UICollectionViewCell()
        }
        return UICollectionViewCell()
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let filteredCars = DataManager.sharedInstance.filteredCars{
            if Array(filteredCars.keys).count > indexPath.row {
                let category = Array(filteredCars.keys)[indexPath.row]
                DataManager.sharedInstance.selectedCategory = category
                reloadView()
                viewModeChangedBlock?(self.currentViewMode)
            }
        }
        
    }
    
}

