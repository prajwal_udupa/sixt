//
//  Constants.swift
//  sixt
//
//  Created by Prajwal Udupa on 05/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import UIKit
enum ViewMode {
    case MapView
    case ListView
}

typealias networkResultBlock = (Dictionary<String, Any>?,Error?)->()
typealias viewModeChangedBlock = (ViewMode) -> ()
typealias contentRefreshRequestedBlock = (Any) -> ()
typealias DetailViewRequestBlock = (Car) -> ()
class Constants{
    static let imageBaseUrl       = "https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/"
    static let imgeSuffixUrl      = "/2x/car.png"
    static let carDetailsUrl      = "http://www.codetalk.de/cars.json"
    static let responseKey        = "response"
    static let fetchingDataLabel  = "Fetching Data..."
    static let failedFetchingData = "Data Not Available. Please retry"
}


class Utilities{
    static func scaleImage(_ originalImage: UIImage, scaledTo size: CGSize) -> UIImage {
        //avoid redundant drawing
        if originalImage.size.equalTo(size) {
            return originalImage
        }
        //create drawing context
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        //draw
        originalImage.draw(in: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(size.width), height: CGFloat(size.height)))
        //capture resultant image
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //return image
        return image!
    }
}
