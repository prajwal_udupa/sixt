//
//  Coordinatable.swift
//  OlaCabs
//
//  Created by Prajwal Udupa on 05/02/17.
//  Copyright © 2016 OlaCabs. All rights reserved.
//

import Foundation
import UIKit
enum ForceCleanTypes:Int {
    case none
    case logout
    case forceUpdate
    case deeplink
}


protocol Coordinatable: class {
    var navController: UINavigationController? {get set}
    var childCoordinators: [Coordinatable] {get set}
    /*
     * ForceHandler will be called on childReceiving forceClean message.
     * Implement onForceCleanHandler for all it's child and
     * remove coordinator from childCoordinators collection
     */
    var onForceCleanHandler: ((_ coordinator: Coordinatable, _ type: ForceCleanTypes)->())? {get set}
    
    init(navigationController: UINavigationController)
    func start()
    func cleanUp()
    func forceClean(type: ForceCleanTypes)
}

extension Coordinatable {
    /*
     * forceClean method is meant to return control back to parent
     * coordinator after performing it's own clean up, if fails to
     * give back control on parent results in undefined state.
     * Hence try to use default implementation provided unless required.
     */
    func forceClean(type: ForceCleanTypes) {
        /*
         * Give child coordinators a chance to clean up
         */
        for cc in childCoordinators {
            cc.forceClean(type: type)
        }
        cleanUp()
        onForceCleanHandler?(self, type)
    }
    
    func deleteChildCoordinator(element: Coordinatable) {
        childCoordinators = childCoordinators.filter() { $0 !== element }
    }
}
