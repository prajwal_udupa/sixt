//
//  FirstImpressionCoordinator.swift
//  sixt
//
//  Created by Prajwal Udupa on 05/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import UIKit

final class FirstImpressionCoordinator: NSObject, Coordinatable {
    var navController: UINavigationController?
    var childCoordinators: [Coordinatable] = []
    var onForceCleanHandler: ((_ coordinator: Coordinatable,_ type: ForceCleanTypes)->())?
    
    
    var listController:ListViewController?
    var mapViewController:MapViewController?
    var firstImpressionController:FirstImpressionController?
    var carData:[Car]?
    init(navigationController: UINavigationController) {
        navController = navigationController
        navController?.isNavigationBarHidden = true
    }
    func getListViewController() -> ListViewController {
        if let controller = listController {
            return controller
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let listViewController = storyboard.instantiateViewController(withIdentifier: "ListViewController") as? ListViewController {
            self.listController = listViewController
            listViewController.detailViewRequestBlock = { car in
                if let detailViewController = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
                    detailViewController.view.layoutIfNeeded()
                    self.navController?.pushViewController(detailViewController, animated: true)
                    detailViewController.updateFor(car: car)
                }
            }
            return listViewController
        }
        self.listController = ListViewController()
        return self.listController ?? ListViewController()
    }
    
    func getMapViewController() -> MapViewController {
        if let controller = mapViewController {
            self.mapViewController = controller
            return controller
        }
        self.mapViewController = MapViewController()
        return self.mapViewController ?? MapViewController()
    }
    
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let firstImpressionController = storyboard.instantiateViewController(withIdentifier: "FirstImpressionController") as? FirstImpressionController {
            self.firstImpressionController = firstImpressionController
            navController?.pushViewController(firstImpressionController, animated: false)
            firstImpressionController.viewModeChangedBlock = { newMode in
                switch newMode {
                case .ListView:
                    firstImpressionController.changeContentControl(controller: self.getListViewController())
                    self.getListViewController().refreshList()
                    break
                case .MapView:
                    firstImpressionController.changeContentControl(controller: self.getMapViewController())
                    self.getMapViewController().refreshMarkers()
                    break
                    
                }
            }
            firstImpressionController.contentRefreshRequestedBlock = { controller in
                DataManager.sharedInstance.fetchCarData { (cars) in
                    self.firstImpressionController?.reloadView()
                    if DataManager.sharedInstance.cars != nil {
                        firstImpressionController.changeContentControl(controller: self.getListViewController())
                        self.listController?.refreshList()
                        self.mapViewController?.refreshMarkers()
                    }
                    else
                    {
                        firstImpressionController.changeToNoResponse()
                    }
                    
                }
            }
            DataManager.sharedInstance.fetchCarData { (cars) in
                self.firstImpressionController?.reloadView()
                if DataManager.sharedInstance.cars != nil {
                    firstImpressionController.changeContentControl(controller: self.getListViewController())
                    self.listController?.refreshList()
                    self.mapViewController?.refreshMarkers()
                }
                else
                {
                    firstImpressionController.changeToNoResponse()
                }
                
            }
            
        }
        
        
    }
    func refreshData() {
        
    }
    func cleanUp() {
        
    }
    
    
}
