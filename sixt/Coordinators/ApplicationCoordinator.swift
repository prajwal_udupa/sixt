//
//  ApplicationCoordinator.swift
//  OlaCabs
//
//  Created by Prajwal Udupa on 05/02/17.
//  Copyright © 2016 OlaCabs. All rights reserved.
//

import Foundation
import UIKit

final class ApplicationCoordinator: NSObject, Coordinatable {
    var navController: UINavigationController?
    var childCoordinators: [Coordinatable] = []
    var onForceCleanHandler: ((_ coordinator: Coordinatable,_ type: ForceCleanTypes)->())?
    
    init(navigationController: UINavigationController) {
        navController = navigationController
        navController?.isNavigationBarHidden = true
    }
    
    func start() {
        let firstImpressionNavController = UINavigationController()
        self.navController?.present(firstImpressionNavController, animated: false, completion: {
            let firstImpressionCoordinator = FirstImpressionCoordinator(navigationController:firstImpressionNavController )
            firstImpressionCoordinator.start()
            self.childCoordinators.append(firstImpressionCoordinator)
        })
    }
    
    func cleanUp() {
        
        
    }
    
    func forceCleanExecutedForType(type: ForceCleanTypes) {
        switch type {
        case .deeplink: break
        case .forceUpdate: fallthrough
        case .logout:break
        default: break
        }
    }
    
    func forceLogout() {
        forceClean(type: .logout)
        
    }
    
}



