//
//  DataManager.swift
//  sixt
//
//  Created by Prajwal Udupa on 06/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import GoogleMaps
class DataManager {
    static let sharedInstance = DataManager()
    private init (){}
    
    var cars:[Car]?{
        didSet {
            filteredCars = [String:[Car]]()
            if let cars = DataManager.sharedInstance.cars {
                for car in cars {
                    if let carArray = filteredCars?[car.modelName]{
                        var carList = carArray
                        carList.append(car)
                        filteredCars?[car.modelName] = carList
                        
                    }
                    else
                    {
                        var carList = [Car]()
                        carList.append(car)
                        filteredCars?[car.modelName] = carList
                        
                    }
                    
                }
            }
        }
    }
    var filteredCars:[String:[Car]]?
    var selectedCategory:String?
    func fetchCarData(completion:@escaping contentRefreshRequestedBlock) -> () {
        _ = NetworkManager.sharedInstance.requestWith(data: Dictionary(), url: Constants.carDetailsUrl, completionBlock: {responseData,error in
            if(error == nil){
                if let response = responseData?[Constants.responseKey] as? NSArray {
                    var cars = [Car]()
                    for car in response {
                        if let data = car as? Dictionary<String, Any> {
                            cars.append(Car(dict: data))
                        }
                    }
                    self.cars = cars
                    completion(cars)
                }
            }
            else
            {
                completion([Car]())
            }
        })
    }
    
    func fetchMarkerImageForCar(car:Car,marker:GMSMarker) {
        if let bigImage = UIImage(named:"Car") {
            let image = Utilities.scaleImage(bigImage , scaledTo:CGSize(width:30,height:30))
            marker.icon  = image
            
        }
        _ = NetworkManager.sharedInstance.requestCarImage(car: car) { dict,error in
            if let imageData = dict?[Constants.responseKey] as? UIImage {
                let bigImage = imageData
                let image = Utilities.scaleImage(bigImage , scaledTo:CGSize(width:30,height:30))
                marker.icon  = image
                
                
            }
        }
    }
    func fetchAndSetCarImage(car:Car,imageView:UIImageView) {
        if let bigImage = UIImage(named:"Car") {
            imageView.image  = bigImage
            
        }
        _ = NetworkManager.sharedInstance.requestCarImage(car: car) { dict,error in
            if let imageData = dict?[Constants.responseKey] as? UIImage {
                imageView.image  = imageData
                
                
            }
        }
        
    }
}
