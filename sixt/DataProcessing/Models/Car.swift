//
//  Car.swift
//  sixt
//
//  Created by Prajwal Udupa on 06/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import CoreLocation
class Car{
    var id:String = ""
    var modelIdentifier:String = ""
    var modelName:String = ""
    var modelMake:String = ""
    var name:String = ""
    var group:String = ""
    var color:String = ""
    var series:String = ""
    var fuelType:String = ""
    var fuelLevel:Int = 0
    var transmission:String = ""
    var licensePlate:String = ""
    var coordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(0, 0)
    var innerCleanliness:String = ""
    var carImageURL:String = ""
    
    convenience init(dict:Dictionary<String, Any>){
        self.init()
        self.id               = (dict["id"] as? String) ?? ""
        self.modelIdentifier  = (dict["modelIdentifier"] as? String) ?? ""
        self.modelName        = (dict["modelName"] as? String) ?? ""
        self.modelMake        = (dict["modelMake"] as? String) ?? ""
        self.name             = (dict["name"] as? String) ?? ""
        self.group            = (dict["group"] as? String) ?? ""
        self.color            = (dict["color"] as? String) ?? ""
        self.series           = (dict["series"] as? String) ?? ""
        self.fuelType         = (dict["fuelType"] as? String) ?? ""
        self.fuelLevel        = (dict["fuelLevel"] as? Int) ?? 0
        self.transmission     = (dict["transmission"] as? String) ?? ""
        self.licensePlate     = (dict["licensePlate"] as? String) ?? ""
        self.innerCleanliness = (dict["innerCleanliness"] as? String) ?? ""
        self.coordinate       = CLLocationCoordinate2DMake(((dict["latitude"] as? CLLocationDegrees) ?? 0), ((dict["longitude"] as? CLLocationDegrees) ?? 0))
        self.carImageURL      =  Constants.imageBaseUrl + self.modelIdentifier + "/" + self.color + Constants.imgeSuffixUrl
    }
    private init(){
        
    }
}
