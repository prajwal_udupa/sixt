//
//  MapViewController.swift
//  sixt
//
//  Created by Prajwal Udupa on 06/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
class MapViewController:UIViewController {
    
    var mapView:GMSMapView?
    override func loadView() {
        let camera = GMSCameraPosition.camera(withLatitude: 12.9111346, longitude: 77.646325, zoom: 12.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        self.mapView = mapView
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func addMarker(car:Car){
        let marker = GMSMarker()
        marker.position = car.coordinate
        marker.title = car.name
        marker.snippet = car.modelName
        DataManager.sharedInstance.fetchMarkerImageForCar(car: car, marker: marker)
        marker.map = mapView
    }
    func refreshMarkers() {
        
        self.mapView?.clear()
        if let cars = DataManager.sharedInstance.filteredCars?[DataManager.sharedInstance.selectedCategory ?? ""] {
            for car in cars {
                addMarker(car: car)
            }
        }
        if let car = DataManager.sharedInstance.filteredCars?[DataManager.sharedInstance.selectedCategory ?? ""]?.first {
            mapView?.animate(toLocation: car.coordinate)
        }
    }
    
}
