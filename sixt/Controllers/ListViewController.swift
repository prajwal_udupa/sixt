//
//  ListViewController.swift
//  sixt
//
//  Created by Prajwal Udupa on 06/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import UIKit
class ListViewController:UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var detailViewRequestBlock:DetailViewRequestBlock?
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if DataManager.sharedInstance.selectedCategory == nil {
            return 0
        }
        return DataManager.sharedInstance.filteredCars?[DataManager.sharedInstance.selectedCategory ?? ""]?.count ?? 0
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let  cell = tableView.dequeueReusableCell(withIdentifier: "CarCell") as? CarCell {
            if let car  = DataManager.sharedInstance.filteredCars?[DataManager.sharedInstance.selectedCategory ?? ""]?[indexPath.row] {
                cell.updateUI(car: car)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let car  = DataManager.sharedInstance.filteredCars?[DataManager.sharedInstance.selectedCategory ?? ""]?[indexPath.row] {
            self.detailViewRequestBlock?(car);
        }
    }
    func refreshList() {
        tableView.reloadData()
    }
}
