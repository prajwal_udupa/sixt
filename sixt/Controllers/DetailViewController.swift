//
//  DetailViewController.swift
//  sixt
//
//  Created by Prajwal Udupa on 06/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import Alamofire
class DetailViewController:UIViewController,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: UIView!
    
    var selectedCar:Car?
    func updateFor(car:Car){
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = "Car Details"
        selectedCar = car
        self.tableView.reloadData()
        self.tableView.showsVerticalScrollIndicator = false
        let camera = GMSCameraPosition.camera(withLatitude: 12.9111346, longitude: 77.646325, zoom: 12.0)
        if let mapView = GMSMapView.map(withFrame: self.mapView.bounds, camera: camera){
            self.mapView.addSubview(mapView)
            if let coordinate = selectedCar?.coordinate {
                let marker = GMSMarker()
                marker.position = coordinate
                marker.title = selectedCar?.modelName
                marker.snippet = selectedCar?.modelMake
                marker.map = mapView
                DataManager.sharedInstance.fetchMarkerImageForCar(car: car, marker: marker)
                mapView.animate(toLocation: coordinate)
                
            }
        }
        
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            self.navigationController?.navigationBar.topItem?.title = ""
            self.navigationController?.isNavigationBarHidden = true
        }
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCell(withIdentifier: "Detail Cell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Detail Cell")
        }
        switch indexPath.row {
        case 0:
            cell?.textLabel?.text = "Model Identifier: " + (selectedCar?.modelIdentifier ?? "")
            break
        case 1:
            cell?.textLabel?.text = "Model Name: " + (selectedCar?.modelName ?? "")
            break
        case 2:
            cell?.textLabel?.text = "Name: " + (selectedCar?.name ?? "")
            break
        case 3:
            cell?.textLabel?.text = "Model Group: " + (selectedCar?.group ?? "")
            break
        case 4:
            cell?.textLabel?.text = "Model Color: " + (selectedCar?.color ?? "")
            break
        case 5:
            cell?.textLabel?.text = "Series: " + (selectedCar?.series ?? "")
            break
        case 6:
            cell?.textLabel?.text = "Fuel Type: " + (selectedCar?.fuelType ?? "")
            break
        case 7:
            cell?.textLabel?.text = "Fuel Level: " + (String(describing: selectedCar?.fuelLevel ?? 0) )
            break
        case 8:
            cell?.textLabel?.text = "Transmission: " + (selectedCar?.transmission ?? "")
            break
        case 9:
            cell?.textLabel?.text = "Cleanliness: " + (selectedCar?.innerCleanliness ?? "")
            break
        case 10:
            cell?.textLabel?.text = "Licence Plate: " + (selectedCar?.licensePlate ?? "")
            break
        default:
            break
        }
        return  cell ?? UITableViewCell()
    }
}
