//
//  FirstImpressionView.swift
//  sixt
//
//  Created by Prajwal Udupa on 14/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import UIKit
enum FirstImpressionViewMode {
    case FetchingContent
    case NoResponse
    case ResponseView
}
protocol FirstImpressionViewProtocol:class {
    func requestContent()
    func viewModeChanged(mode:ViewMode)
}
extension UIView {
    func rotate360Degrees(duration: CFTimeInterval = 1.0) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = CGFloat(M_PI * 2.0)
        rotateAnimation.toValue = 0.0
        rotateAnimation.duration = duration
        self.layer.add(rotateAnimation, forKey: nil)
    }
}
class FirstImpressionView: UIView {
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var errorImage: UIImageView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    weak var delegate:FirstImpressionViewProtocol?
    @IBAction func segmentChanged(_ sender: Any) {
        if self.segmentControl.selectedSegmentIndex == 0 {
            self.delegate?.viewModeChanged(mode: .ListView)
        }
        else
        {
            self.delegate?.viewModeChanged(mode: .MapView)
        }
    }
    
    var viewMode:FirstImpressionViewMode {
        didSet {
            switch viewMode {
            case .FetchingContent:
                self.errorLabel.text = Constants.fetchingDataLabel
                self.errorImage.rotate360Degrees()
                self.contentView.isHidden = true
                self.gestureRecognizers = nil
                self.segmentControl.isEnabled = false
                self.segmentControl.isUserInteractionEnabled = false
                break
            case .NoResponse:
                self.errorLabel.text = Constants.failedFetchingData
                self.contentView.isHidden = true
                self.segmentControl.isEnabled = false
                self.segmentControl.isUserInteractionEnabled = false
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.requestContent(gesture:)))
                tapGesture.numberOfTapsRequired = 1
                self.addGestureRecognizer(tapGesture)
            case .ResponseView:
                self.contentView.isHidden = false
                self.segmentControl.isEnabled = true
                self.segmentControl.isUserInteractionEnabled = true
                break
            }
        }
    }
    required init?(coder aDecoder: NSCoder) {
        self.viewMode = .FetchingContent
        super.init(coder: aDecoder)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.isHidden = true
        self.errorLabel.text = Constants.fetchingDataLabel
        self.errorImage.rotate360Degrees(duration: 1)
    }
    
    func updateInterfaceWithView(view:UIView) {
        self.viewMode = .ResponseView
        view.frame = self.bounds
        self.contentView.addSubview(view)
    }
    func requestContent(gesture:UITapGestureRecognizer) {
        self.delegate?.requestContent()
    }
    
}
