//
//  File.swift
//  sixt
//
//  Created by Prajwal Udupa on 06/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class CarCell: UITableViewCell {
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var licenceLabel: UILabel!
    func updateUI(car:Car) {
        carImageView.layer.cornerRadius = carImageView.frame.size.height/2
        DataManager.sharedInstance.fetchAndSetCarImage(car: car, imageView: carImageView)
        carImageView.layer.backgroundColor = UIColor.lightGray.cgColor
        categoryLabel.text = car.modelName + " - " + car.name
        licenceLabel.text = car.licensePlate
    }
}
