//
//  CategoryCell.swift
//  sixt
//
//  Created by Prajwal Udupa on 06/02/17.
//  Copyright © 2017 Prajwal. All rights reserved.
//

import Foundation
import UIKit
class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    override func awakeFromNib() {
        self.borderView.layer.cornerRadius = self.borderView.bounds.size.width/2
        self.borderView.layer.borderWidth = 0.5
        self.borderView.layer.borderColor = UIColor.lightGray.cgColor
    }
    func setupWithData(category:String) {
        self.categoryLabel.text = category
        if category == DataManager.sharedInstance.selectedCategory {
            self.borderView.layer.backgroundColor = UIColor.cyan.cgColor
            
        }
        else
        {
            self.borderView.layer.backgroundColor = UIColor.white.cgColor
        }
    }
}
